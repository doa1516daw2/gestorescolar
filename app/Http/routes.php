<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/',[
    'as' => 'index',
    'uses' => 'ShowController@index'
]);

Route::get('/insert',[
    'as' => 'insert',
    'uses' => 'EditableController@add'
]);
Route::get('/notas',[
    'as' => 'notas',
    'uses' => 'ShowController@getNotas'
]);
Route::get('/notaEdit', [
    'as' => 'nota.edit',
    'uses' => 'EditableController@notaEdit'
]);

Route::post('/change', 'EditableController@setNota');
Route::post('/insertDataCurso', 'EditableController@setCurso');
Route::post('/insertDataAssignatura', 'EditableController@setAssignatura');
Route::post('/insertDataAlumno', 'EditableController@setAlumno');
Route::match(['get', 'post'], '/alumnonota', 'ShowController@verNotas');
Route::match(['get', 'post'], '/aprovado', 'ShowController@aprovado');
Route::get('/matriculados', 'ShowController@matriculado');
Route::get('/cursos', 'ShowController@getCurso');
Route::get('/assignaturas', 'ShowController@getAssignatura');
Route::get('/alumnos', 'ShowController@getAlumno');