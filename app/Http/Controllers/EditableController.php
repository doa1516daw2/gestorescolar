<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class EditableController extends Controller
{

    public function setCurso(Request $peticion)
    {
        if(count($peticion -> input('nombre')) == 1) {
            $query = DB::insert('INSERT INTO cursos(id, nombre) VALUES (NULL,?)', [$peticion->input('nombre')[0]]);
            if (!$query) {
                return view('error');
            } else {
                return view('success');
            }
        } else {
            DB::beginTransaction();
            for($i = 0; $i < count($peticion -> input('nombre')); $i++)
            {
                DB::insert('INSERT INTO cursos(id, nombre) VALUES (NULL,?)', [$peticion->input('nombre')[$i]]);
            }
            DB::commit();
            return view('success');
        }
    }

    public function add()
    {
        $query = DB::select('SELECT nombre FROM cursos');
        return view('insert', ['estado' => Input::get('a'), 'cursos' => $query]);

    }


    public function setAssignatura(Request $peticion)
    {
        if(count($peticion -> input('nombre')) == 1)
        {
            $query = DB::insert('INSERT INTO asignaturas(id, nombre, curso_id) VALUES (NULL,?,(SELECT id FROM cursos WHERE nombre = ?))',
                [$peticion->input('nombre')[0], $peticion->input('curso')]);
            if (!$query) {
                return view('error');
            } else {
                return view('success');
            }
        } else {
            DB::beginTransaction();
            for($i = 0; $i < count($peticion -> input('nombre')); $i++)
            {
                DB::insert('INSERT INTO asignaturas(id, nombre, curso_id) VALUES (NULL,?,(SELECT id FROM cursos WHERE nombre = ?))',
                    [$peticion->input('Nombre')[$i], $peticion->input('Curso')]);
            }
            DB::commit();
            return view('success');
        }
    }


    public function setAlumno(Request $peticion)
    {
        $Assignaturas = DB::select('SELECT id FROM asignaturas WHERE curso_id = (SELECT id FROM cursos WHERE nombre = ?)', [$peticion -> input('curso')]);
        if(!$Assignaturas)
        {
            return view('error');
        }
        DB::beginTransaction();
        DB::insert('INSERT INTO alumnos(id, nombre, apellido, DNI) VALUES (NULL, ?, ?, ?)', [$peticion->input('nombre'), $peticion->input('apellido'), $peticion->input('DNI')]);
        DB::insert('INSERT INTO matriculados(curso_id, alumno_id) VALUES ((
                      SELECT id FROM cursos WHERE nombre = ?
                    ), (
                      SELECT id FROM alumnos WHERE DNI = ?
                    ))', [$peticion -> input('curso'), $peticion -> input('DNI')]);
        $ID_alumno = DB::select('SELECT id FROM alumnos WHERE DNI = ?', [$peticion->input('DNI')]);
        foreach ($Assignaturas as $Assignatura) {
            DB::insert('INSERT INTO notas(asignatura_id, alumno_id, nota) VALUES (?,?, NULL)', [$Assignatura->id, $ID_alumno[0]->id]);
        }
        DB::commit();
        return view('success');
    }

    public function setNota(Request $peticion)
    {
        $items = array(
            $peticion -> input('DNI'),
            $peticion -> input('cursos'),
            $peticion -> input('asignatura'),
            $peticion -> input('notas')
        );
        DB::beginTransaction();
        for($i = 0; $i < count($items[1]); $i++)
        {
            DB::update('UPDATE notas SET nota = ? WHERE asignatura_id =
                      (SELECT id FROM asignaturas WHERE curso_id = ? AND nombre = ?)
                      AND alumno_id = (SELECT id FROM alumnos WHERE DNI = ?)',
                [$items[3][$i], $items[1][$i], $items[2][$i], $items[0]]);
        }
        DB::commit();
        return view('success');
    }

    public function notaEdit()
    {
        $query = DB::select('SELECT alumnos.DNI AS DNI, alumnos.nombre AS alumno,
                        asignaturas.curso_id AS curso, notas.nota AS nota,
                        asignaturas.nombre AS asignatura FROM notas
                        JOIN alumnos ON alumnos.id = notas.alumno_id
                        JOIN asignaturas ON asignaturas.id = notas.asignatura_id
                        WHERE alumnos.id = ?', [Input::get('id')]);

        return view('notaEdit', ['alumno' => $query]);

    }
}