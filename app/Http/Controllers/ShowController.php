<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Imagick;
use ImagickDraw;
use App\Http\Controllers\Controller;

class ShowController extends Controller
{

    public function index()
    {
        return view('index');

    }

    public function getNotas()
    {
        $query = DB::select('SELECT * FROM alumnos');
        return view('nota', ['alumnos' => $query]);

    }

    public function getCurso()
    {
        $query = DB::select('SELECT * FROM cursos');
        return view('tablaDatos', ['cursos' => $query, 'estado' => 1]);
    }


    public function getAssignatura()
    {
        $query = DB::select('SELECT asignaturas.id AS "id", asignaturas.nombre AS "nombre", cursos.nombre AS "curso"
                              FROM asignaturas JOIN cursos ON asignaturas.curso_id = cursos.id');
        return view('tablaDatos', ['assignaturas' => $query, 'estado' => 2]);
    }


    public function getAlumno()
    {
        $query = DB::select('SELECT * FROM alumnos');
        return view('tablaDatos', ['alumnos' => $query, 'estado' => 3]);
    }


    public function verNotas(Request $peticion)
    {
        if($peticion -> input('alumno') == NULL ){
            $Alumnos = DB::select('SELECT id, nombre, apellido FROM alumnos');
            return view('grafico', ['alumnos' => $Alumnos, 'estado' => 2]);
        }
        $values = array();
        $Alumnos = DB::select('SELECT id, nombre, apellido FROM alumnos');
        $consulta = DB::select('SELECT asignaturas.nombre AS referencia, notas.nota as valor FROM notas
                                JOIN asignaturas ON asignaturas.id = notas.asignatura_id
                                WHERE notas.alumno_id = ?', [$peticion -> input('alumno')]);
        for($i = 0; $i < count($consulta); $i ++){
            $values[$i] = array($consulta[$i] -> referencia, (int)$consulta[$i] -> valor);
        }
        $this->genGrafica($values,10);
        return view('grafico', ['alumnos' => $Alumnos, 'estado' => 2]);
    }


    public function aprovado(Request $peticion){
        if($peticion -> input('curso') == NULL ){
            $cursos = DB::select('SELECT nombre FROM cursos');
            return view('grafico', ['cursos' => $cursos, 'estado' => 1]);
        }
        $cursos = DB::select('SELECT nombre FROM cursos');
        $consulta_Aprovado = DB::select('SELECT  "aprovado" AS referencia, count(*) as valor from notas
                                JOIN asignaturas ON asignaturas.id = notas.asignatura_id
                                WHERE notas.nota >= 5 AND asignaturas.curso_id =
                                (SELECT id FROM cursos WHERE nombre = ?)
                                ', [$peticion -> input('curso')]);
        $consulta_Suspendido = DB::select('SELECT  "suspendido" AS referencia, count(*) as valor from notas
                                JOIN asignaturas ON asignaturas.id = notas.asignatura_id
                                WHERE notas.nota < 5 AND asignaturas.curso_id =
                                (SELECT id FROM cursos WHERE nombre = ?)
                                ', [$peticion -> input('curso')]);

        $values[0] = array($consulta_Aprovado[0] -> referencia, (int)$consulta_Aprovado[0] -> valor);
        $values[1] = array($consulta_Suspendido[0] -> referencia, (int)$consulta_Suspendido[0] -> valor);
        if($values[0][1] > $values[1][1]){
            $this->genGrafica($values,$values[0][1]);
        } else {
            $this->genGrafica($values,$values[1][1]);
        }
        return view('grafico', ['cursos' => $cursos, 'estado' => 1]);
    }


    public function matriculado(){
        $values = array();
        $max = 0;
        $consulta = DB::select('SELECT cursos.nombre AS referencia, count(alumno_id) AS valor
                                FROM matriculados JOIN cursos ON cursos.id = matriculados.curso_id
                                GROUP BY curso_id ');
        for($i = 0; $i < count($consulta); $i ++){
            $values[$i] = array($consulta[$i] -> referencia, (int)$consulta[$i] -> valor);
            if($max < $values[$i][1]){
                $max = $values[$i][1];
            }
        }
        $this -> genGrafica($values, $max);
        return view('grafico', ['estado' => 3]);
    }

    public function genGrafica($values, $max)
    {
            $section = 800 / count($values);
            $x = array(0, $section);
            $colorMemoria = '';
            $color = '';
            //Genera el grafico
            $draw = new ImagickDraw();
            $draw->setStrokeColor('black');
            $draw->setStrokeWidth(2);
            for ($z = 0; $z < count($values); $z++) {
                while (1) {
                    $color = '#';
                    $color .= dechex(rand(0, 255));
                    $color .= dechex(rand(0, 255));
                    $color .= dechex(rand(0, 255));
                    while (strlen($color) != 7) {
                        $color .= '0';
                    }
                    if (strpos($colorMemoria, $color) === false) {
                        $colorMemoria .= $color;
                        break;
                    }
                }
                $draw->setFillColor($color);
                $values[$z][2] = $color;
                $y = ((($values[$z][1] * 100) / $max) * 500) / 100;
                $draw->rectangle($x[0], 500 - $y, $x[1], 500);
                $x[0] = $x[1];
                $x[1] += $section;
            }
            $imagick = new Imagick();
            $imagick->newImage(800, 500, 'white');
            $imagick->setImageFormat("png");
            $imagick->drawImage($draw);
            $imagick->writeImage('img/Grafico.png');
            $imagick->destroy();


            //Genera leyenda
            $posInit = array(20, count($values) * 40);
            $draw = new ImagickDraw();
            $draw->setStrokeColor('black');
            $draw->setStrokeWidth(1);
            for ($z = 0; $z < count($values); $z++) {
                $draw->setFillColor($values[$z][2]);
                $draw->rectangle($posInit[0], $posInit[1] - 20, $posInit[0] - 20, $posInit[1]);
                $posInit[0] = 30;
                $draw->setFontSize(20);
                $draw->setFillColor('black');
                $draw->annotation($posInit[0], $posInit[1], $values[$z][0] . ' : ' . $values[$z][1]);
                $posInit[1] = (count($values) - $z - 1) * 40;
                $posInit[0] = 20;
            }
            $imagick = new Imagick();
            $imagick->newImage(400, 20 + count($values) * 40, 'white');
            $imagick->setImageFormat("png");
            $imagick->drawImage($draw);
            $imagick->writeImage('img/Leyenda.png');
            $imagick->destroy();
    }
}