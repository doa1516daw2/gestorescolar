@extends('headerData')

@section('contenido')
    @if($estado == 1)
        <script src="{{asset('/js/IterativeCurso.js')}}"></script>

        <form action="/insertDataCurso" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label>Nombre:</label>
                <div class="col-sm-5 ">
                    <input type="text" name="nombre[]" class="form-control">
                </div>
            </div>

            <span class="glyphicon glyphicon-plus" aria-hidden="true" id="add"></span>
            <div id="iterativeAssignatura"></div>
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    <input type="submit" class="btn btn-default">
                </div>
            </div>

        </form>

    @elseif($estado == 2)
        <script src="{{ asset('/js/IterativeAssignatura.js') }}"></script>

        <form action="/insertDataAssignatura" method="post">
            {{csrf_field()}}

            <div class="form-group">
                <label>curso: </label>
                <div class="col-sm-5 ">
                    <select id="Curso" name="curso" class="form-control">
                        @foreach($cursos as $curso)
                            <option value="{{ $curso -> nombre }}">{{ $curso -> nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label> Nombre</label>
                <div class="col-sm-5">
                    <input type="text" name="nombre[]">
                </div>
            </div>
            <span class="glyphicon glyphicon-plus" aria-hidden="true" id="add"></span>
            <div id="iterativeAssignatura"></div>
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">

                    <input type="submit" class="btn btn-default">

                </div>
            </div>


        </form>
    @elseif($estado == 3)

        <form action="/insertDataAlumno" method="post">
            {{csrf_field()}}

            <div class="form-group">
                <label>Curso</label>
                <div class="col-sm-5 ">
                    <select id="Curso" name="curso" class="form-control">
                        @foreach($cursos as $curso)
                            <option value="{{ $curso -> nombre }}">{{ $curso -> nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label>DNI</label>
                <div class="col-sm-5 ">
                    <input type="text" name="DNI" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label>Nombre </label>
                <div class="col-sm-5 ">
                    <input type="text" name="nombre" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label>Apellido:</label>
                <div class="col-sm-5 ">
                    <input type="text" name="apellido" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    <input type="submit" class="btn btn-default">
                </div>
            </div>
        </form>

    @else
    @endif
@endsection