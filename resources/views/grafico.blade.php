
@extends('headerData')

@section('contenido')
    @if($estado == 1)
        <form action="/aprovado" method="post">
            {{csrf_field()}}
            Curso: <select id="Curso" name="curso" class="form-control">
                @foreach($cursos as $curso)
                    <option value="{{ $curso -> nombre }}">{{ $curso -> nombre }}</option>
                @endforeach
            </select>

            <input type="submit" class="btn btn-default">

        </form>
    @elseif($estado == 2)
        <form action="/alumnonota" method="post">
            {{csrf_field()}}
            Curso: <select id="Alumno" name="alumno" class="form-control">
                @foreach($alumnos as $alumno)
                    <option value="{{ $alumno -> id }}">{{ $alumno -> apellido }}, {{ $alumno -> nombre }}</option>
                @endforeach
            </select>

            <input type="submit" class="btn btn-default">

        </form>
    @endif
    <img src="{{asset('img/Grafico.png')}}" style="display: block;margin: 0 auto;"/>
    <img src="{{asset('img/Leyenda.png')}}" style="display: block;margin: 0 auto;"/>
@endsection