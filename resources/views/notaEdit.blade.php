@extends('headerData')

@section('contenido')

    <form action="/change" method="post">
        {{csrf_field()}}
        <input type="hidden" name="DNI" value="{{$alumno[0] ->DNI}}">
        <table class="table table-hover">
            @foreach($alumno as $item)
                <tr>
                    <th>Alumno</th>
                    <th>Asignatura</th>
                    <th>Nota</th>
                </tr>
                <tr>
                    <th>{{$item -> alumno}}</th>
                    <td>
                        {{$item -> asignatura}}
                        <input type="hidden" name="asignatura[]" value="{{$item -> asignatura}}">
                        <input type="hidden" name="cursos[]" value="{{$item -> curso}}">
                    </td>
                    <td>
                        <input type="number" name="notas[]" value="{{$item -> nota}}" min="0" max="10">
                    </td>
                </tr>
            @endforeach
        </table>
        <input type="submit" class="btn btn-default">


    </form>
@endsection