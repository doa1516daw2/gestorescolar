
@extends('headerData')

@section('contenido')
    <table class="table table-hover">
        <tr><th>DNI</th><th>Nombre</th><th>Apellido</th></tr>
        @foreach($alumnos as $alumno)
            <tr><th><a href="/notaEdit?id={{$alumno -> id}}">{{ $alumno -> DNI }}</a></th>
                <td><a href="/notaEdit?id={{$alumno -> id}}">{{ $alumno -> nombre }}</a></td>
                <td><a href="/notaEdit?id={{$alumno -> id}}">{{ $alumno -> apellido }}</a></td></tr>
        @endforeach
    </table>
@endsection