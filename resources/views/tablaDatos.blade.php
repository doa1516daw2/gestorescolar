@extends('headerData')

@section('contenido')
    @if($estado == 1)
        <table class="table table-hover">
            <tr><th>#</th><th>nombre</th></tr>
            @foreach($cursos as $item)
                <tr><th>{{$item -> id}}</th><td>{{$item -> nombre}}</td></tr>
            @endforeach
        </table>
    @elseif($estado == 2)
        <table class="table table-hover">
            <tr><th>#</th><th>nombre</th><th>curso</th></tr>
            @foreach($assignaturas as $item)
                <tr><th>{{$item -> id}}</th><td>{{$item -> nombre}}</td><td>{{$item -> curso}}</td></tr>
            @endforeach
        </table>
    @elseif($estado == 3)
        <table class="table table-hover">
            <tr><th>#</th><th>DNI</th><th>Nombre</th><th>Apellido</th></tr>
            @foreach($alumnos as $item)
                <tr><th>{{$item -> id}}</th><td>{{$item -> DNI}}</td><td>{{$item -> nombre}}</td><td>{{$item -> apellido}}</td></tr>
            @endforeach
        </table>
    @else
    @endif
@endsection